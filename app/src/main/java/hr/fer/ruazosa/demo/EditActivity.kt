package hr.fer.ruazosa.demo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_edit.*

class EditActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)

        if (DemoModel.firstName != null) {
            firstNameEditText.setText(DemoModel.firstName)
        }
        if (DemoModel.lastName != null) {
            lastNameEditText.setText(DemoModel.lastName)
        }

        saveFirstAndLastNameButton.setOnClickListener {
            DemoModel.firstName = firstNameEditText.text.toString()
            DemoModel.lastName = lastNameEditText.text.toString()

            finish()
        }
    }
}
