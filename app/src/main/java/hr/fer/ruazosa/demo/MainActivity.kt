package hr.fer.ruazosa.demo

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        firstNameTextView.text = DemoModel.firstName
        lastNameTextView.text = DemoModel.lastName

        editFirstAndLastNameButton.setOnClickListener {
            val startEditActivity = Intent(this, EditActivity::class.java)
            startActivity(startEditActivity)
        }
    }

    override fun onResume() {
        super.onResume()
        firstNameTextView.text = DemoModel.firstName
        lastNameTextView.text = DemoModel.lastName
    }
}
